# Brushed Dyes

A fun little Resource Pack that changes Minecraft's Dyes into Paint Brushes

## Images

![preview1](assets/preview1.png)
![preview2](assets/preview2.png)

## Usage

The Resource Pack should be compatible with all* versions of Minecraft

Tested Versions:
- 1.18.2
- 1.12.2

*If you find a version that isn't compatible, please create an issue!

## Assets

Do you like the textures of this Resource Pack? You can use them in your own project as long as your usage conforms with the project's [license](https://gitlab.com/sorrowfall/brushed-dyes/-/blob/main/license)